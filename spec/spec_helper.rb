require 'rubygems'
require 'appium_lib'
require 'em/pure_ruby'
require 'rspec'
require 'selenium-webdriver'
require_relative 'support/fc_setup'
require_relative 'support/login'
require_relative 'support/nav'
require_relative 'support/settings'
require_relative 'support/addressbook'
require_relative 'support/cb_setup'
require_relative 'support/callbridge'

RSpec.configure do |config|
	config.include SetupHelpers
	config.include LoginHelpers
	config.include NavHelpers
	config.include SettingsHelpers
	config.include ContactHelpers
	config.include CBSetupHelpers
	config.include CallbridgeHelpers
end