RSpec.describe 'Settings Test: Credit Card Errors', type: :feature do
	$tname = 'Appium'
	$temail = 'joey@iotum.com'
	$tpassword = 'tester'
	before(:all) do
		driverSetup
	end
	it 'Charge is declined with processing error' do
		sleep(4)
		sign_up_and_load_settings($tname, $temail, $tpassword)
		scrollToBottom
		add_credit_card('4000000000000119','1225','245','25413','Appium','iotum','Toronto','Canada')
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]/android.view.View[1]','InvalCreditCardError')
	end
	it 'Charge is declined with incorrect cvc code' do
		sleep(4)
		sign_up_and_load_settings($tname, $temail, $tpassword)
		scrollToBottom
		add_credit_card('4000000000000127','1225','245','25413','Appium','iotum','Toronto','Canada')
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]/android.view.View[1]','InvalCVCError')
	end
	it 'Charge is declined with insufficient funds' do
		sleep(4)
		sign_up_and_load_settings($tname, $temail, $tpassword)
		scrollToBottom
		add_credit_card('4000000000009995','1225','245','25413','Appium','iotum','Toronto','Canada')
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]/android.view.View[1]','InsuffFundsError')
	end
	it 'Charge is declined with card expired' do
		sleep(4)
		sign_up_and_load_settings($tname, $temail, $tpassword)
		scrollToBottom
		add_credit_card('4000000000000069','1225','245','25413','Appium','iotum','Toronto','Canada')
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]/android.view.View[1]','ExpiredCardError')
	end
	it 'Charge is declined with lost card' do
		sleep(4)
		sign_up_and_load_settings($tname, $temail, $tpassword)
		scrollToBottom
		add_credit_card('4000000000009979','1225','245','25413','Appium','iotum','Toronto','Canada')
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]/android.view.View[1]','LostCardError')
	end
	it 'Charge is declined with incorrect card number' do
		sleep(4)
		sign_up_and_load_settings($tname, $temail, $tpassword)
		scrollToBottom
		add_credit_card('4242424242424241','1225','245','25413','Appium','iotum','Toronto','Canada')
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]/android.view.View[1]','IncorCardError')
	end
	it 'Invalid card with no card number' do
		sleep(4)
		sign_up_and_load_settings($tname, $temail, $tpassword)
		scrollToBottom
		add_credit_card('','1225','245','25413','Appium','iotum','Toronto','Canada')
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]/android.view.View[1]','NoCardNumError')
	end
	it 'Invalid card with no expiry date' do
		sleep(4)
		sign_up_and_load_settings($tname, $temail, $tpassword)
		scrollToBottom
		add_credit_card('424242424242424','','245','25413','Appium','iotum','Toronto','Canada')
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]/android.view.View[1]','NoExpError')
	end
	it 'Invalid card with invalid date' do
		sleep(4)
		sign_up_and_load_settings($tname, $temail, $tpassword)
		scrollToBottom
		add_credit_card('424242424242424','1205','245','25413','Appium','iotum','Toronto','Canada')
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]/android.view.View[1]','InvalExpError')
	end
	it 'Invalid card with no cvc' do
		sleep(4)
		sign_up_and_load_settings($tname, $temail, $tpassword)
		scrollToBottom
		add_credit_card('424242424242424','1225','','25413','Appium','iotum','Toronto','Canada')
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]/android.view.View[1]','NoCVCError')
	end
	it 'Invalid card with no postal' do
		sleep(4)
		sign_up_and_load_settings($tname, $temail, $tpassword)
		scrollToBottom
		add_credit_card('424242424242424','1225','245','','Appium','iotum','Toronto','Canada')
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]/android.view.View[1]','NoPostError')
	end
	it 'Valid card with no Name' do
		sleep(4)
		sign_up_and_load_settings($tname, $temail, $tpassword)
		scrollToBottom
		add_credit_card('424242424242424','1225','245','25413','','iotum','Toronto','Canada')
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]/android.view.View[1]','NoNameError')
	end
	it 'Valid card with no address' do
		sleep(4)
		sign_up_and_load_settings($tname, $temail, $tpassword)
		scrollToBottom
		add_credit_card('424242424242424','1225','245','25413','Appium','','Toronto','Canada')
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]/android.view.View[1]','NoAddressError')
	end
	it 'Valid card with no city' do
		sleep(4)
		sign_up_and_load_settings($tname, $temail, $tpassword)
		scrollToBottom
		add_credit_card('424242424242424','1225','245','25413','Appium','iotum','','Canada')
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]/android.view.View[1]','NoCityError')
	end
	it 'Valid card with no country' do
		sleep(4)
		sign_up_and_load_settings($tname, $temail, $tpassword)
		scrollToBottom
		add_credit_card('424242424242424','1225','245','25413','Appium','iotum','','Canada')
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]/android.view.View[1]','NoCountryError')
	end
	it 'Successful add card' do
		sleep(4)
		sign_up_and_load_settings($tname, $temail, $tpassword)
		scrollToBottom
		add_credit_card('424242424242424','1225','245','25413','Appium','iotum','Toronto','Canada')
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]','CardUpdateSuccess')
	end
	after :each do
		back
		scrollToBottom
		delete_account
	end
end
