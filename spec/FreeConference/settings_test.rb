RSpec.describe 'Settings Test: Contact Details', type: :feature do
	$tname = 'Appium'
	$temail = 'joey@iotum.com'
	$tpassword = 'tester'
	before :all do
		driverSetup
	end
	it 'Save contact w/out name' do
		sleep(2)
		sign_up_and_load_settings($tname,$temail,$tpassword)
		contact_details('','joey@iotum.com')
		sleep(2)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[3]','NoNameContactError')
	end
	#it 'Save contact w/out email' do
	#	sign_up_and_load_settings($tname,$temail,$tpassword)
	#	contact_details('NoEmail','')
	#	sleep(2)
	#	check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[3]','NoEmailContactError')
	#end
	#it 'Save contact with invalid email' do
	#	sign_up_and_load_settings($tname,$temail,$tpassword)
	#	contact_details('InvalEmail','fakeemail.com')
	#	sleep(2)
	#	check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[3]','InvalEmailContactError')
	#end 
	#it 'Save contact succesfully' do
	#	sign_up_and_load_settings($tname,$temail,$tpassword)
	#	contact_details('Appium','joey@iotum.com')
	#	sleep(2)
	#	check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[3]','SavedContactSuccess')
	#end
	after :each do
		back
		sleep(1)
		scrollToBottom
		sleep(1)
		delete_account
	end
end
RSpec.describe 'Settings Test: Change Password', type: :feature do
	before(:all) do
		driverSetup
	end
	
	it 'Enter without password' do
		sign_up_and_load_settings($tname,$temail,$tpassword)
		sleep(1)
		change_password('','',true)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]','PasswordIsReqError')
	end
	it 'Enter few chars' do
		sign_up_and_load_settings($tname,$temail,$tpassword)
		sleep(1)
		change_password('test','test',true)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]','NotEnoughCharsError')
	end
	it 'No confirm password' do
		sign_up_and_load_settings($tname,$temail,$tpassword)
		sleep(1)
		change_password('tester','',true)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]','NoSecondPWError')
	end
	it 'Passwords dont match' do
		sign_up_and_load_settings($tname,$temail,$tpassword)
		sleep(1)
		change_password('tester','testerer',true)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]','PWDontMatchError')
	end
	it 'Valid password info' do
		sign_up_and_load_settings($tname,$temail,$tpassword)
		sleep(1)
		change_password('tester','tester',true)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]','SuccessPWChange')
	end
	after :each do
		back
		sleep(1)
		scrollToBottom
		sleep(1)
		delete_account
	end
end
RSpec.describe 'Settings Test: Change Access Code', type: :feature do
	before(:all) do
		driverSetup
	end
	it 'Update Access Code' do
		sign_up_and_load_settings($tname,$temail,$tpassword)
		getNewAccessCode
	end
	after :each do
		back
		sleep(1)
		scrollToBottom
		sleep(1)
		delete_account
	end
end
RSpec.describe 'Settings Test: Change Moderator Pin', type: :feature do
	before(:all) do
		driverSetup
	end
	it 'Update Mod Pin' do
		sign_up_and_load_settings($tname,$temail,$tpassword)
		getNewModPin
	end
end
RSpec.describe 'Settings Test: Change Profile Photo', type: :feature do
	before(:all) do
		driverSetup
	end
	it 'Upload unsupported image' do
		sleep(2)
		sign_up_and_load_settings($tname,$temail,$tpassword)
		change_picture('tiffex.tiff')
		sleep(1)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]','UnsupportIMGError')
	end
	it 'Upload PNG' do
		sign_up_and_load_settings($tname,$temail,$tpassword)
		change_picture('beegphoto.png')
		sleep(1)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]','SuccessPNGProfile')
	end
	it 'Upload GIF' do
		sign_up_and_load_settings($tname,$temail,$tpassword)
		change_picture('fyDA.gif')
		sleep(1)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]','SuccessGIFProfile')
	end
	it 'Upload JPEG' do
		sign_up_and_load_settings($tname,$temail,$tpassword)
		change_picture('beeg.jpg')
		sleep(1)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]','SuccessJPEGProfile')
	end
	after :each do
		back
		sleep(1)
		scrollToBottom
		sleep(1)
		delete_account
	end
end
RSpec.describe 'Settings Test: Pinless entry and SMS notifications', type: :feature do
	before(:all) do
		driverSetup
	end
	it 'Adding a pinless phone number and disabling sms notifications' do
		sleep(2)
		sign_up_and_load_settings($tname,$temail,$tpassword)
		PinlessEntryAndSMS('Netherlands (+31)','6135550163',false)
	end
	after :each do
		back
		sleep(1)
		scrollToBottom
		sleep(1)
		delete_account
	end
end
RSpec.describe 'Settings Test: Disable Hold Music', type: :feature do
	before(:all) do
		driverSetup
	end
	it 'Disabling Iotum Original Composition' do
		sleep(2)
		sign_up_and_load_settings($tname,$temail,$tpassword)
		disable_holdmusic
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]','SuccessChangeHoldMusic')
	end
	after :each do
		back
		sleep(1)
		scrollToBottom
		sleep(1)
		delete_account
	end
end
RSpec.describe 'Settings Test: Enable Waiting Room', type: :feature do
	before(:all) do
		driverSetup
	end
	it 'Enable Waiting Room' do
		sleep(4)
		sign_up_and_load_settings($tname,$temail,$tpassword)
		enable_waitingroom
	end
	after :each do
		back
		sleep(1)
		scrollToBottom
		sleep(1)
		delete_account
	end
end
RSpec.describe 'Settings Test: Disable Chimes and Name Announce', type: :feature do
	before(:all) do
		driverSetup
	end
	it 'Disabling Chimes and Name Announce' do
		sleep(4)
		sign_up_and_load_settings($tname,$temail,$tpassword)
		chimesnameannounce(true,true)
	end
	after :each do
		back
		sleep(1)
		scrollToBottom
		sleep(1)
		delete_account
	end
end
RSpec.describe 'Settings Test: Calendar Invite', type: :feature do
	before(:all) do
		driverSetup
	end
	it 'Enabling Calendar email invites' do
		sleep(4)
		sign_up_and_load_settings($tname,$temail,$tpassword)
		invitations('calendar')
	end
	after :each do
		back
		sleep(1)
		scrollToBottom
		sleep(1)
		delete_account
	end
end
RSpec.describe 'Settings Test: Moderator Controls', type: :feature do
	before(:all) do
		driverSetup
	end
	it 'Setting Mod Control to QandA' do
		sleep(4)
		sign_up_and_load_settings($tname,$temail,$tpassword)
		scrollToBottom
		modControls('QandA')
	end
	it 'Settings Mod Control to Presentation' do
		sleep(4)
		sign_up_and_load_settings($tname,$temail,$tpassword)
		scrollToBottom
		modControls('presentation')
	end
	after :each do
		back
		sleep(1)
		scrollToBottom
		sleep(1)
		delete_account
	end
end

#CREDIT CARD TESTING DOES NOT WORK AT THE MOMENT- : 

#NEED TO ADD A STREAMING SETTINGS , SECURITY CODE