RSpec.describe 'Settings Test: Address Book: Add Contact', type: :feature do
	$tname = 'appium'
	$temail = 'joey@iotum.com'
	$tpassword = 'tester'
	before :all do
		driverSetup
	end
	#THIS CURRENTLY DOES NOT WORK(FACE-2743)
	it 'Add Contact w/o a name' do
		sleep(10)
		sign_up($tname,$temail,$tpassword,true)
		sleep(14)
		add_contact('','jlmartin9000@gmail.com')
		check_displayed_xpath('','NoNameContactError')
	end
	it 'Add Contact w/o an email' do
		add_contact('','jlmartin9000@gmail.com')
		check_displayed_xpath('','NoEmailContactError')
	end
	it 'Add Contact with inval email' do
		add_contact('Name','hello.freeconference.com')
		check_displayed_xpath('','NoEmailContactError')
	end
	it 'Succesful add contact' do
		add_contact('Name','jlmartin9000@gmail.com')
		check_displayed_xpath('','NoEmailContactError')
	end
	after :each do
		back
	end
	after :all do
		settings
		sleep(2)
		scrollToBottom
		delete_account
	end
end