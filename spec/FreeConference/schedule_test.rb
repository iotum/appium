RSpec.describe 'Schedule One time call on free account', type: :feature do
	before :all do
		driverSetup
	end
	it 'One time regular access code on free account' do
		schedule_mode('Test description测试')
		schedule('','')
		select_free_dial_ins
		schedule_call
	end
	after :all do
		settings
		sleep(2)
		scrollToBottom
		sleep(1)
		delete_account
	end
end
RSpec.describe 'Schedule Recurring Calls', type: :feature do
	before :all do
		driverSetup
	end
	it 'Repeat Daily' do
		schedule_mode('Repeated Daily测试')
		repeat_daily
		schedule('','jlmartin9000@gmail.com')
		select_free_dial_ins
		schedule_call
	end
	it 'Repeat every weekday' do
		schedule_mode('Repeated every weekday测试')
		repeat_weekday
		schedule('','jlmartin9000@gmail.com')
		select_free_dial_ins
		schedule_call
	end
	after :each do
		sleep(2)
		settings
		sleep(2)
		scrollToBottom
		sleep(1)
		delete_account
		sleep(1)
	end
end
