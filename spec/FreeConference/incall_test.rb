RSpec.describe 'FC Test: In Call Page', type: :feature do
	$tname = 'appium'
	$temail = 'joey@iotum.com'
	$tpassword = 'tester'
	before :all do
		driverSetup
	end
	it 'In Call Page Test' do
		sleep(14)
		sign_up($tname,$temail,$tpassword,true)
		sleep(10)
		joinACall
		sleep(2)
		Appium::Core::TouchAction.new(@driver).tap(x: 800, y: 1170, duration: 1000).perform
		sleep(3)
		find_element(:xpath, '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.view.View[2]/android.view.View[1]/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View[2]/android.view.View').click
		#Giving FC Access to photos,files and camera
		sleep(1)
		Appium::Core::TouchAction.new(@driver).tap(x: 800, y: 1200, duration: 1000).perform
		sleep(1)
		Appium::Core::TouchAction.new(@driver).tap(x: 800, y: 1200, duration: 1000).perform
		check_incall
	end
	after (:all) do
		sleep(4)
		end_call
		sleep(2)
		settings
		sleep(2)
		scrollToBottom
		sleep(1)
		delete_account
	end
end