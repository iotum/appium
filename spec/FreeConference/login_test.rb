RSpec.describe 'Test: Display sign up messages', type: :feature do
	before (:all) do
		driverSetup
	end
	tname = 'Appium'
	temail = 'joey@iotum.com'
	tpassword = 'tester'
	it 'Forget to accept the terms and policy' do
		sleep(10)
		sign_up(tname,temail,tpassword, false)
		sleep(2)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[1]/android.view.View[2]/android.view.View', 'termsError')
		back
	end
	it 'Forget to enter a name' do
		sleep(10)
		sign_up('', temail,tpassword,true)
		sleep(2)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[1]/android.view.View[2]/android.view.View','nameError')
	end
	it 'Forget to enter an email' do
		sleep(10)
		sign_up(tname, '', tpassword, true)
		sleep(2)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[1]/android.view.View[2]/android.view.View','emailError')
	end
	it 'Forget to enter a password' do
		sleep(10)
		sign_up(tname, temail, '', true)
		sleep(2)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[1]/android.view.View[2]/android.view.View','passwordError')
	end
	it 'Use an existing email' do
		sleep(10)
		sign_up(tname, 'jlmartin9000@gmail.com', tpassword, true)
		sleep(2)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[1]/android.view.View[2]/android.view.View','sameEmailError')
		sleep(1)
	end
end
RSpec.describe 'Test: Sign Up', type: :feature do
	before (:all) do
		driverSetup
	end
	it 'Sign up via email' do
		#sleep(10)
		sign_up('Appium', 'joey@iotum.com', 'tester', true)
		#sleep(8)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[3]', 'startAConference')
	end
	#FACEBOOK LOGIN IS NOT WORKING AT THE MOMENT- BUT TO TEST IT ONE WOULD USE THE FOLLOWING
#	it 'Sign up via Facebook' do
#		sleep(10)
#		signup_fb
#		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[3]', 'startAConference')
#	end
	after (:all) do
	settings
	sleep(2)
	scrollToBottom
	sleep(1)
	delete_account
	end
end
RSpec.describe 'Test: Login', type: :feature do
	before (:all) do
		driverSetup
	end
	it 'login with an invalid password' do
	#	sleep(10)
		login('jlmartin9000@gmail', 'testerer')
	#	sleep(2)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[1]','InvalUserNm|PW')
	end
	it 'Valid login' do
		#sleep(10)
		login('jlmartin9000@gmail.com', 'tester')
		#sleep(8)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[3]', 'StartAConference')
	end
end