module CBSetupHelpers
	def cbDriverSetup
		desired_caps = 
			{ 
				caps:
					{ 
					platformName: 'Android',
					platformVersion: '9',
					deviceName: 'Google Pixel 3',
					appActivity: 'com.iotum.callbridge.MainActivity',
					appPackage: 'com.iotum.callbridge',
					avd: 'Pixel3'
					}
			}
			
		RSpec.configure { | c |
			c.before(:each) {
								@driver = Appium::Driver.new(desired_caps, true)
								@driver.start_driver
								Appium.promote_appium_methods Object
								manage.timeouts.implicit_wait = 16
							}
						}
		puts "Driver has started\n"
	end
	#below is from dashboard
	def joinACall
		sleep(16)
		find_element(:xpath, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[4]").click
		sleep(13)
		find_element(:xpath, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.view.View[3]/android.widget.Button[1]").click
	end                       
end