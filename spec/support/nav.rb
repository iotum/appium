module NavHelpers
	def settings
		find_element(:xpath, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View[1]").click
	end
	def upgrade
		find_element(:xpath, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View[3]").click
	end
	def sidebar 
		find_element(:xpath, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[2]/android.view.View/android.view.View").click
	end
	def conference
		find_element(:xpath, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View[2]").click
	end
	def scrollToBottom
		Appium::TouchAction.new.swipe(start_x: 144, start_y: 1634, end_x: 144, end_y: 149).perform
	end
	def sign_up_and_load_settings(name, email, password)
		sleep(10)
		sign_up(name,email,password,true)
		sleep(15)
		settings
		sleep(5)
	end
	def check_displayed_xpath(xpath, name)
		if find_element(:xpath, xpath).displayed? == true
			puts "Passed! Element #{name} is displayed!\n"
		end
		rescue Selenium::WebDriver::Error::NoSuchElementError
			puts "Failed! Element #{name} isn't displayed!\n"
	end
	def check_incall
		if find_element(:xpath,'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.view.View[2]/android.view.View[1]/android.view.View[3]/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View').displayed? == true
			puts "Join Call Button displayed! Participant not in Conference!\n"
		end
		rescue Selenium::WebDriver::Error::NoSuchElementError
			puts "Join Call Button not displayed! Participant In conference!\n"
	end
	def check_video_feed
		if find_element(:xpath,'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.widget.Button').visible? == true
			puts "Participant is sharing video feed!\n"
		end
		rescue Selenium::WebDriver::Error::NoSuchElementError
			puts "Participant is not sharing video feed!\n"
	end
end
