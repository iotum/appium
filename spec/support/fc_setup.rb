module SetupHelpers
	def driverSetup
		desired_caps = 
			{ 
				caps:
					{ 
					platformName: 'Android',
					platformVersion: '9',
					deviceName: 'Google Pixel 3',
					appActivity: 'com.freeconference.next.MainActivity',
					appPackage: 'com.freeconference.next',
					avd: 'Pixel3'
					}
			}
			
		RSpec.configure { | c |
			c.before(:each) {
								@driver = Appium::Driver.new(desired_caps, true)
								@driver.start_driver
								Appium.promote_appium_methods Object
								manage.timeouts.implicit_wait = 16
							}
						}
		puts "Driver has started\n"
	end
	#below is from dashboard
	def joinACall
		sleep(14)
		find_element(:xpath, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[4]").click
		sleep(11)
		find_element(:xpath, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.view.View[3]/android.widget.Button[1]").click
	end         
	def end_call
		#this is in the in-call page, takes you to dashboard
		Appium::Core::TouchAction.new(@driver).tap(x: 731, y: 335, duration: 1000).perform
		sleep(1)
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.view.View[2]/android.view.View[1]/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View[4]/android.view.View").click
		sleep(1)
		Appium::Core::TouchAction.new(@driver).tap(x: 900, y: 1930, duration: 1000).perform
		sleep(1)
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.widget.Button").click
	end
end