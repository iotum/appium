require_relative 'login'
module CallSetupHelpers
	def schedule_mode(description)
		sleep(12)
		sign_up('appium','joey@iotum.com','tester',true)
		sleep(15)
		find_element(:xpath, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[5]").click
		sleep(4)
		find_element(:xpath, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.view.View[3]/android.view.View[1]/android.widget.EditText").send_keys('Appium Automated Test')
		call_description(description)
	end
	def start_mode
		find_element(:xpath, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[4]").click
	end
	def call_description(desc)
		if desc != ''
			find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.view.View[3]/android.view.View[5]/android.widget.EditText").send_keys(desc)
		end
	end
	def repeat_daily
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.view.View[3]/android.view.View[6]/android.widget.Button[1]").click
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.widget.Button[2]").click
	end
	def repeat_weekday
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.view.View[3]/android.view.View[6]/android.widget.Button[1]").click
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.view.View[3]/android.view.View[3]/android.view.View/android.view.View/android.widget.RadioButton").click
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.widget.Button[2]").click
	end
	def schedule(pName,pEmail)
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.widget.Button[2]").click
		if pName != '' || pEmail != ''
		add_participant(pName,pEmail)
		end
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.widget.Button[2]").click
	end
	def add_participant(name,email)
		if name != '' && email != ''
			puts 'You can only input one type of identifier for a particpant'
		elsif name != ''
			input = name
		else
			input = email
		end
		find_element(:xpath, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.view.View[3]/android.view.View[1]/android.view.View[2]/android.view.View/android.view.View/android.view.View[2]/android.widget.EditText").send_keys(input)
		sleep(1)
		press_keycode(66)
		find_element(:xpath, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.view.View[3]/android.view.View[3]/android.view.View[2]/android.widget.Button").click
		sleep(1)
	end
	def select_free_dial_ins
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.view.View[3]/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View").click
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.widget.Button[2]").click
		#find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.widget.Button[2]").click
		find_element(:xpath, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.widget.Button[2]").click
	end
	def schedule_call
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.widget.Button[2]").click
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.view.View[3]/android.view.View[1]/android.view.View[2]/android.view.View','CallHasBeenScheduled')
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.widget.Button").click
	end
	def preview
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]/android.widget.Button").click
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[4]/android.view.View/android.view.View[2]").click
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.widget.Button").click
	end
	def edit_upcoming_call(title,desc)
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[2]/android.widget.Button").click
		sleep(1)
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View[2]/android.view.View[4]/android.view.View/android.view.View[1]").click
		sleep(1)             
		#the following is to check whether the call that is being edited is repeating or not
		justThisCall = find_element(:xpath,'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.widget.Button[3]')
		if justThisCall.displayed? == true
			find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.widget.Button[2]").click
			sleep(1)
		end
		if title != ''
			find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.view.View[3]/android.view.View[1]/android.widget.EditText").send_keys(title)
		end
		if desc != ''
			find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.view.View[3]/android.view.View[5]/android.widget.EditText").send_keys(desc)
		end
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.widget.Button[2]").click
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.widget.Button[2]").click
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.widget.Button[2]").click
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.widget.Button[2]").click
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.widget.Button[2]").click
		find_element(:xpath,"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View/android.widget.Button").click
	end
end