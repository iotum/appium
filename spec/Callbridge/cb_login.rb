RSpec.describe 'Test: Display login messages', type: :feature do
	$tcbemail = 'joey@iotum.com'
	$tcbpassword = 'tester'
	before :each do
		cbDriverSetup
	end
	it 'No email address / username' do
		sleep(8)
		cb_login('iotum','',$tcbpassword)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[1]','NoEmailError')
	end
	it 'No password' do
		sleep(5)
		cb_login('iotum',$tcbemail,'')
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[1]','NoPasswordError')
	end
	it 'login with invalid email' do
		sleep(5)
		cb_login('iotum','moc.elgoog',$tcbpassword)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[1]','InvalEmailError')
	end
	it 'Login with invalid password' do
		sleep(5)
		cb_login('iotum',$tcbemail,'tester!')
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[1]','InvalPasswordError')
	end
	it 'Succesful login' do
		sleep(5)
		cb_login('iotum',$tcbemail,$tcbpassword)
		check_displayed_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[5]/android.view.View[2]/android.view.View[3]','LoginSuccess')
	end
end